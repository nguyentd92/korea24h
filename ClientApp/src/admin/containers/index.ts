import { ProductListComponent } from './product-list/product-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';

export const containers: any[] = [
    ProductListComponent,
    DashboardComponent
]

export * from './product-list/product-list.component';
export * from './dashboard/dashboard.component';
