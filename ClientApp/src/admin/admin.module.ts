import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminRoutingModule } from "./admin-routing.module";

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule
} from "@coreui/angular";
import { AdminLayoutComponent } from "./layout/admin-layout.component";
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import * as fromAuth from './auth';
import * as fromContainers from './containers';
import { DashboardComponent } from './containers/dashboard/dashboard.component'

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule,
    TabsModule,
    ChartsModule
  ],
  declarations: [
    AdminLayoutComponent,
    ...fromAuth.pages,
    ...fromContainers.containers,
    DashboardComponent
  ]
})
export class AdminModule {}
