import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/admin',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Quản lý bán hàng'
  },
  {
    name: 'Đơn hàng mới',
    url: '/admin/products',
    icon: 'icon-drop',
    badge: {
      variant: 'warning',
      text: 'HOT'
    }
  },
  {
    name: 'Sản phẩm',
    url: '/admin/products',
    icon: 'icon-drop'
  },
  {
    name: 'Doanh thu',
    url: '/theme/typography',
    icon: 'icon-pencil'
  },
  {
    title: true,
    name: 'Quản lý nội dung'
  },
  {
    name: 'Banner',
    url: '/admin/products',
    icon: 'icon-drop',
    badge: {
      variant: 'warning',
      text: 'HOT'
    }
  },
  {
    name: 'Quảng cáo',
    url: '/admin/products',
    icon: 'icon-drop',
    badge: {
      variant: 'warning',
      text: 'HOT'
    }
  },
  {
    title: true,
    name: 'Marketing'
  },
  {
    name: 'Khách hàng',
    url: '/admin/products',
    icon: 'icon-drop',
    badge: {
      variant: 'warning',
      text: 'HOT'
    }
  },
  {
    name: 'Quảng cáo',
    url: '/admin/products',
    icon: 'icon-drop',
    badge: {
      variant: 'warning',
      text: 'HOT'
    }
  },
  {
    title: true,
    name: 'Dữ liệu hệ thống'
  },
  {
    name: 'Từ điển dữ liệu',
    url: '/base',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Ngành hàng',
        url: '/base/cards',
        icon: 'icon-puzzle'
      },
      {
        name: 'Thể loại',
        url: '/base/carousels',
        icon: 'icon-puzzle'
      },
      {
        name: 'Thuơng hiệu',
        url: '/base/carousels',
        icon: 'icon-puzzle'
      },
      {
        name: 'Địa phuơng',
        url: '/base/carousels',
        icon: 'icon-puzzle'
      },
    ]
  },
  {
    name: 'Cấu hình',
    url: '/buttons',
    icon: 'icon-cursor',
    children: [
      {
        name: 'Chúng tôi',
        url: '/buttons/buttons',
        icon: 'icon-cursor'
      },
      {
        name: 'Liên hệ',
        url: '/buttons/dropdowns',
        icon: 'icon-cursor'
      },
    ]
  },
  {
    name: 'Charts',
    url: '/charts',
    icon: 'icon-pie-chart'
  },
  {
    name: 'Icons',
    url: '/icons',
    icon: 'icon-star',
    children: [
      {
        name: 'CoreUI Icons',
        url: '/icons/coreui-icons',
        icon: 'icon-star',
        badge: {
          variant: 'success',
          text: 'NEW'
        }
      },
      {
        name: 'Flags',
        url: '/icons/flags',
        icon: 'icon-star'
      },
      {
        name: 'Font Awesome',
        url: '/icons/font-awesome',
        icon: 'icon-star',
        badge: {
          variant: 'secondary',
          text: '4.7'
        }
      },
      {
        name: 'Simple Line Icons',
        url: '/icons/simple-line-icons',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Notifications',
    url: '/notifications',
    icon: 'icon-bell',
    children: [
      {
        name: 'Alerts',
        url: '/notifications/alerts',
        icon: 'icon-bell'
      },
      {
        name: 'Badges',
        url: '/notifications/badges',
        icon: 'icon-bell'
      },
      {
        name: 'Modals',
        url: '/notifications/modals',
        icon: 'icon-bell'
      }
    ]
  },
  {
    name: 'Widgets',
    url: '/widgets',
    icon: 'icon-calculator',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Extras',
  },
  {
    name: 'Pages',
    url: '/pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: '/login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: '/register',
        icon: 'icon-star'
      },
      {
        name: 'Error 404',
        url: '/404',
        icon: 'icon-star'
      },
      {
        name: 'Error 500',
        url: '/500',
        icon: 'icon-star'
      }
    ]
  },
  // {
  //   name: 'Disabled',
  //   url: '/dashboard',
  //   icon: 'icon-ban',
  //   badge: {
  //     variant: 'secondary',
  //     text: 'NEW'
  //   },
  //   attributes: { disabled: true },
  // },
  // {
  //   name: 'Download CoreUI',
  //   url: 'http://coreui.io/angular/',
  //   icon: 'icon-cloud-download',
  //   class: 'mt-auto',
  //   variant: 'success',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // },
  // {
  //   name: 'Try CoreUI PRO',
  //   url: 'http://coreui.io/pro/angular/',
  //   icon: 'icon-layers',
  //   variant: 'danger',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // }
];
