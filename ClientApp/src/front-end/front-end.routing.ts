import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './pages';

const FRONT_END_ROUTES: Routes = [
  {
    path: '',
    component: HomePage,
    pathMatch: 'full'
  }
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FRONT_END_ROUTES)
  ],
  exports: [RouterModule]
})
export class FrontEndRoutingModule { }
