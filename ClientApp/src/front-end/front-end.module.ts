import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrontEndRoutingModule } from './front-end.routing';
import * as fromPages from './pages'

@NgModule({
  imports: [
    CommonModule,
    FrontEndRoutingModule
  ],
  declarations: [...fromPages.pages],

})
export class FrontEndModule { }
